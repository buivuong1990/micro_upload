var express = require("express");
var knex = require("../config/knex");
var router = express.Router();
var jwt = require('jsonwebtoken');
var orderid = require('order-id')('mysecret');
var request = require("request");


router.post('/user/cart/sync', function(req, res){
    var email = req.body.email;
    var prev_email = req.body.prev_email;

    knex('users')
    .select('id')
    .where('email', prev_email)
    .asCallback(function(err1, prev_users){
        const prev_id = prev_users[0].id;
        if(err1) return res.status(500).json();
        knex('users')
        .select('id')
        .where('email', email)
        .asCallback(function(err2, users){
            const id = users[0].id;
            if(err2) return res.status(500).json();
            knex('carts')
            .select('id', 'device_id')
            .where('user_id', prev_id)
            .asCallback(function(err3, prevCarts){
                if(err3) return res.status(500).json();
                knex('carts')
                .select('id', 'device_id')
                .where('user_id', id)
                .asCallback(function(err4, carts){
                    if(err4) return res.status(500).json();
                    var updateCarts = [];
                    var removeCarts = [];
                    for(var i = 0; i < prevCarts.length; i++){
                        var prevCart = prevCarts[i];
                        var flag = false;
                        for(var j = 0; j < carts.length; j++){
                            var cart = carts[j];
                            if(cart.device_id == prevCart.device_id){
                                flag = true;
                                break;
                            }
                        }
                        if(!flag)
                            updateCarts.push(prevCart.id);
                        else
                            removeCarts.push(prevCart.id);
                    }
                    if(removeCarts.length > 0){
                        var where = '';
                        for(var i = 0; i < removeCarts.length; i++){
                            if(i == 0)
                                where += 'id = '+removeCarts[i];
                            else
                                where += ' OR id = '+removeCarts[i];
                        }
                        knex.raw(
                            `DELETE FROM carts
                            WHERE `+where
                        ).then(() => {})
                    }
                    if(updateCarts.length > 0){
                        var where = '';
                        for(var i = 0; i < updateCarts.length; i++){
                            if(i == 0)
                                where += 'id = '+updateCarts[i];
                            else
                                where += ' OR id = '+updateCarts[i];
                        }
                        knex.raw(
                            `UPDATE carts
                            SET user_id = `+id+`
                            WHERE `+where
                        ).then(() => {
                            res.json({status: 200});    
                        })
                        .catch(() => {
                            res.status(500).json();
                        })
                    }else{
                        res.status(200).json({status: 200});
                    }
                })
            })
        })
    })
});


module.exports = router;